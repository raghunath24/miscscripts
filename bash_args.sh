#!/bin/bash
#echo "$@"
# things to take as input
# 1. workspacewith PATH
# 2. HOSTNAME
# 3. apikey
# 4. engagment id

#values="/home/raghu/Documents -host=abcd -api=abcde -engagement=1"

#split the workspace arguements
#and pass the command to scan
IFS=' '
read -ra ADDR <<< "$@"

#make sure array is not empty
if [[ "${ADDR[@]}" ]]
then
    echo "gosec --fmt=json --out=fileabc.json ${ADDR[0]}"

    if [[ -f fileabc.json && "${ADDR[@]:1}" ]]
    then
	#check if the above command is successfully or file exisits
	#split the workspace arguments
	#check for host/api etc and other parameters wether in place or not
	#check the arguments before executing the command
	#check first if the file exists
	echo "python upload-results.py ${ADDR[@]:1}"
    else
	echo "Missing upload arguments or results file"
    fi
else
    echo "Please provide Arguments"
fi
