#!/bin/bash

#bareminium setup for the vagrant ubuntu xenial 64

#updatecache
sudo apt-get update

#mypreferred toolset
sudo apt-get install emacs-nox git


#setting up language prefs
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

sudo apt-get install -y language-pack-en 

sudo locale-gen


#Additional
#sudo apt-get dist-upgrade
